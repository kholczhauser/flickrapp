package com.holczhauser.flickrapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.BindingAdapter;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.holczhauser.flickrapplication.backend.FlickrImpl;
import com.holczhauser.flickrapplication.backend.converter.FlickResponseConverter;
import com.holczhauser.flickrapplication.ui.ImageViewModel;
import com.squareup.picasso.Picasso;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<ResponseBody> {

    private static final int REQUEST_CODE = 651;
    private ImageViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkRequiredPermission();

        FrameLayout containerLayout = (FrameLayout) findViewById(R.id.container_layout);

        RecyclerView.LayoutManager layoutManager;
        if (getResources().getBoolean(R.bool.is_tablet)) {
            layoutManager = new GridLayoutManager(getApplicationContext(), getResources().getInteger(R.integer.column_count));
        } else {
            layoutManager = new LinearLayoutManager(getApplicationContext());
        }
        viewModel = new ImageViewModel(containerLayout, layoutManager, getLayoutInflater());
        new FlickrImpl().getRecentPhotos().enqueue(this);
    }

    private void checkRequiredPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(this, getString(R.string.permission_info), Toast.LENGTH_SHORT).show();

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE);

        }
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        viewModel.loadData(new FlickResponseConverter().convert(response));
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        viewModel.loadError();
    }

    @BindingAdapter("android:src")
    public static void downloadPhoto(ImageView imageView, String url) {
        Picasso.with(imageView.getContext()).load(url).into(imageView);
    }
}