package com.holczhauser.flickrapplication.ui;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.holczhauser.flickrapplication.R;
import com.holczhauser.flickrapplication.adapter.ImageAdapter;

import java.util.List;

public class ImageViewModel implements ViewModelCommunicator {
    private final RecyclerView recycleView;
    private final ProgressBar loadingBar;
    private final LayoutInflater layoutInflater;
    private final FrameLayout rootView;

    public ImageViewModel(FrameLayout rootView, RecyclerView.LayoutManager layoutManager, LayoutInflater inflater) {
        this.rootView = rootView;
        this.layoutInflater = inflater;
        recycleView = (RecyclerView) rootView.findViewById(R.id.imageRecyclerView);
        recycleView.setLayoutManager(layoutManager);
        loadingBar = (ProgressBar) rootView.findViewById(R.id.loadingBar);
    }

    @Override
    public void loadData(List<DumbPhoto> dataList) {
        loadingBar.setVisibility(View.GONE);
        recycleView.setVisibility(View.VISIBLE);
        if (dataList != null) {
            recycleView.setAdapter(new ImageAdapter(layoutInflater, dataList));
        }
    }

    @Override
    public void loadError() {
        loadingBar.setVisibility(View.VISIBLE);
        recycleView.setVisibility(View.GONE);
        Snackbar.make(rootView, R.string.error_message, Snackbar.LENGTH_SHORT).show();
    }
}
