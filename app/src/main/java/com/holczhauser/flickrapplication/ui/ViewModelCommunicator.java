package com.holczhauser.flickrapplication.ui;

import java.util.List;

public interface ViewModelCommunicator {

    void loadData(List<DumbPhoto> dataList);

    void loadError();


}
