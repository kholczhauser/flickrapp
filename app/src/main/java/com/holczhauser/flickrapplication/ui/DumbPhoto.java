package com.holczhauser.flickrapplication.ui;

import com.holczhauser.flickrapplication.backend.response.recentphotos.Photo;

/**
 * This class represent the simplified version of Photo response.
 * Used to display items on UI.
 */
public class DumbPhoto {
    private String id;
    private String secret;
    private String server;
    private String title;
    private int farm;

    public DumbPhoto(Photo photoItem) {
        this.id = photoItem.getId();
        this.secret = photoItem.getSecret();
        this.server = photoItem.getServer();
        this.title = photoItem.getTitle();
        this.farm = photoItem.getFarm();
    }

    /**
     * Return an URL for 'm' sized image based on farm,server,id,secret params.
     *
     * @return direct URL to m sized image.
     */
    public String getPhotoUrl() {
        return "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_m.jpg";
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }
}
