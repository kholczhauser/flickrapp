package com.holczhauser.flickrapplication.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.widget.ImageView;

import com.holczhauser.flickrapplication.ui.DumbPhoto;

import java.io.File;
import java.io.FileOutputStream;

public class AppUtil {

    public static void openBrowser(Context context, String url) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    public static void saveFile(ImageView imageView, DumbPhoto photo) {
        if (isExternalStorageWritable()) {
            File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TigerSpike");
            if (!root.exists()) {
                root.mkdirs();
            }
            File file = new File(root, photo.getId() + ".jpg");
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* Checks if external storage is available for read and write */
    static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static void shareItem(Context context, String photoId) {
        String path = getImagePath(context, photoId);
        if (path != null && !path.isEmpty()) {
            context.startActivity(Intent.createChooser(generateEmailIntent(path), "Sharing Options"));
        }
    }

    private static Intent generateEmailIntent(String path) {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("image/jpeg");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{""});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "TigerSpike's attachment");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hello, please check the attachment!");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + path));
        return emailIntent;
    }

    private static String getImagePath(Context context, String photoId) {
        if (isExternalStorageReadable()) {
            return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                    + "/" + "TigerSpike"
                    + "/" + photoId;
        }
        return null;
    }

}
