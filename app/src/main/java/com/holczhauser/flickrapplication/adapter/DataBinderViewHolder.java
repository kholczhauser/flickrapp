package com.holczhauser.flickrapplication.adapter;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

public class DataBinderViewHolder extends RecyclerView.ViewHolder {

    private final ViewDataBinding viewDataBinding;

    DataBinderViewHolder(ViewDataBinding viewDataBinding) {
        super(viewDataBinding.getRoot());
        this.viewDataBinding = viewDataBinding;
    }

    public ViewDataBinding getViewDataBinding() {
        return viewDataBinding;
    }
}
