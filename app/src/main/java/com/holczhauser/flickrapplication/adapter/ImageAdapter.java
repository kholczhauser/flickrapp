package com.holczhauser.flickrapplication.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.databinding.library.baseAdapters.BR;
import com.holczhauser.flickrapplication.R;
import com.holczhauser.flickrapplication.app.AppUtil;
import com.holczhauser.flickrapplication.ui.DumbPhoto;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter implements OnImageItemClickListener {
    private final LayoutInflater inflater;
    private final List<DumbPhoto> dataList;

    public ImageAdapter(LayoutInflater inflater, List<DumbPhoto> data) {
        super();
        this.inflater = inflater;
        this.dataList = data;
    }

    @Override
    public DataBinderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataBinderViewHolder(DataBindingUtil.inflate(inflater, R.layout.card_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((DataBinderViewHolder) holder).getViewDataBinding().setVariable(BR.photo, dataList.get(position));
        ((DataBinderViewHolder) holder).getViewDataBinding().setVariable(BR.callback, this);
        ((DataBinderViewHolder) holder).getViewDataBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return dataList == null ? 0 : dataList.size();
    }

    @Override
    public void openBrowser(String url) {
        AppUtil.openBrowser(inflater.getContext(), url);
    }

    @Override
    public void saveFile(View view, DumbPhoto photo) {
        AppUtil.saveFile((ImageView) view, photo);
    }

    @Override
    public void shareItem(String photoId) {
        AppUtil.shareItem(inflater.getContext(), photoId);
    }
}