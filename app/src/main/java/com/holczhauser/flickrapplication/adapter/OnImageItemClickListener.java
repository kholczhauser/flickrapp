package com.holczhauser.flickrapplication.adapter;

import android.view.View;

import com.holczhauser.flickrapplication.ui.DumbPhoto;

/**
 * Callback interface for item click event
 */
public interface OnImageItemClickListener {
    void openBrowser(String url);

    void saveFile(View imageView, DumbPhoto photo);

    void shareItem(String photoId);
}