
package com.holczhauser.flickrapplication.backend.response.recentphotos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Response class, representing Flickr response for RecentPhotos query.
 */
public class RecentPhotosResponse {

    @SerializedName("photos")
    @Expose
    private Photos photos;
    @SerializedName("stat")
    @Expose
    private String stat;

    /**
     * No args constructor for use in serialization
     */
    public RecentPhotosResponse() {
    }

    /**
     * @param photos
     * @param stat
     */
    public RecentPhotosResponse(Photos photos, String stat) {
        this.photos = photos;
        this.stat = stat;
    }

    /**
     * @return The photos
     */
    public Photos getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    /**
     * @return The stat
     */
    public String getStat() {
        return stat;
    }

    /**
     * @param stat The stat
     */
    public void setStat(String stat) {
        this.stat = stat;
    }


}
