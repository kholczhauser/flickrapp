package com.holczhauser.flickrapplication.backend;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;

public class FlickrImpl implements FlickrServer {
    private static final String API_KEY = "2c6e08e11db7385a9b0385e4ba3f74a3";
    private static final String BASE_URL = "https://api.flickr.com/";
    private final FlickrServer service;

    public FlickrImpl() {
        OkHttpClient httpClient = setUpHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL).client(httpClient)
                .build();

        service = retrofit.create(FlickrServer.class);
    }

    private OkHttpClient setUpHttpClient() {
        OkHttpClient.Builder httpClientBuilder =
                new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("api_key", API_KEY)
                        .addQueryParameter("format", "json")
                        .build();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        return httpClientBuilder.build();
    }

    @Override
    public Call<ResponseBody> getRecentPhotos() {
        return service.getRecentPhotos();
    }
}
