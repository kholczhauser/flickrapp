package com.holczhauser.flickrapplication.backend;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

interface FlickrServer {
    @GET("services/rest/?method=flickr.photos.getRecent")
    Call<ResponseBody> getRecentPhotos();
}
