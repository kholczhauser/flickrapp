package com.holczhauser.flickrapplication.backend.converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.holczhauser.flickrapplication.backend.response.recentphotos.Photo;
import com.holczhauser.flickrapplication.backend.response.recentphotos.RecentPhotosResponse;
import com.holczhauser.flickrapplication.ui.DumbPhoto;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class FlickResponseConverter {

    /**
     * Convert Flickr JSON getRecentPhotos response to POJO object
     *
     * @param response - provided by Flicker
     * @return List of DumbPhotos or empty list (in case of empty response or exception).
     */
    public List<DumbPhoto> convert(Response<ResponseBody> response) {
        List<DumbPhoto> dumbPhotoList = new LinkedList<>();
        if (response.body() != null) {
            try {
                String jsonResponse = response.body().string();
                jsonResponse = jsonResponse.replace("jsonFlickrApi", "");
                jsonResponse = jsonResponse.substring(1, jsonResponse.length() - 1);
                Gson gson = new Gson();
                RecentPhotosResponse result = gson.fromJson(jsonResponse, new TypeToken<RecentPhotosResponse>() {
                }.getType());

                if (result != null && result.getStat().equalsIgnoreCase("ok")) {
                    List<Photo> photoList = result.getPhotos().getPhoto();

                    for (Photo photoItem : photoList) {
                        dumbPhotoList.add(new DumbPhoto(photoItem));
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dumbPhotoList;
    }
}
