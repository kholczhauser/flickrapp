package com.holczhauser.flickrapplication;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.holczhauser.flickrapplication.adapter.DataBinderViewHolder;
import com.holczhauser.flickrapplication.adapter.ImageAdapter;
import com.holczhauser.flickrapplication.backend.response.recentphotos.Photo;
import com.holczhauser.flickrapplication.ui.DumbPhoto;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DataBindingUtil.class, LayoutInflater.class})
public class ImageAdapterTest {
    @Mock
    private LayoutInflater inflater;

    @Mock
    private List<DumbPhoto> data;

    @Mock
    private View itemView;

    @Mock
    private DataBinderViewHolder holder;

    @Mock
    private ViewGroup parent;

    @Mock
    private Context context;

    private ImageAdapter sut;

    @Mock
    private ViewDataBinding viewDataBinding;

    @Before
    public void setUp() throws Exception {
        mockStatic(LayoutInflater.class);
        mockStatic(DataBindingUtil.class);

        when(LayoutInflater.class, "from", eq(context)).thenReturn(inflater);
        when(inflater.inflate(anyInt(), any(ViewGroup.class), anyBoolean())).thenReturn(itemView);
        when(holder.getViewDataBinding()).thenReturn(viewDataBinding);
        when(viewDataBinding.getRoot()).thenReturn(itemView);
        when(DataBindingUtil.inflate(eq(inflater), anyInt(), any(ViewGroup.class), anyBoolean())).thenReturn(viewDataBinding);

        sut = new ImageAdapter(inflater, data);
    }

    @Test
    public void testCreateDataBinderViewHolder() {
        RecyclerView.ViewHolder viewHolder = sut.onCreateViewHolder(parent, 0);
        assertTrue(viewHolder instanceof DataBinderViewHolder);
    }

    @Test
    public void testItemCount() {
        data = new ArrayList<>();

        sut = new ImageAdapter(inflater, data);
        Assert.assertEquals(0, sut.getItemCount());

        data = new ArrayList<>();
        data.add(new DumbPhoto(new Photo()));

        sut = new ImageAdapter(inflater, data);
        Assert.assertEquals(1, sut.getItemCount());
    }

    @Test
    public void testOnBindViewHolder() {
        sut.onBindViewHolder(holder, 0);
        verify(holder, times(3)).getViewDataBinding();
        verify(viewDataBinding, times(2)).setVariable(anyInt(), Matchers.anyObject());
        verify(viewDataBinding, times(1)).executePendingBindings();
    }
}